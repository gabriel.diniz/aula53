public class aula53 { // arrays multidimensionais

    public static void main(String[] args) {

        int [] [] lista = new int [5] [5];  // array 2 dimensões
        // cada dimensão 5 elementos
        lista [0][0] = 78; // cada posição recebe o valor
        lista [1][5] = 15; // cada posição recebe o valor
        lista [2][4] = 66; // cada posição recebe o valor
        lista [3][3] = 0; // cada posição recebe o valor
        lista [4][1] = -89; // cada posição recebe o valor
    }
}
